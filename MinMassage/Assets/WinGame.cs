﻿using UnityEngine;
using System.Collections;

public class WinGame : MonoBehaviour 
{
    public GameObject WinParcticles;
    public string Side = "One";

    public delegate void PlayerWin();
    public static event PlayerWin Win;

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player" && col.GetComponent<PlayerController2>().PlayerName == Side && col.GetComponent<PlayerController2>().Role == PlayerRoles.Attacker)
        {
            //  Display win UI
            Instantiate(WinParcticles, Vector3.zero, Quaternion.identity);
            Win();
        }
    }
}
