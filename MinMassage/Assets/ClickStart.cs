﻿using UnityEngine;
using System.Collections;

public class ClickStart : MonoBehaviour 
{
    void Start()
    {
        Time.timeScale = 0f;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetButton("JumpOne") || Input.GetButton("JumpTwo"))
        {
            gameObject.active = false;
            Time.timeScale = 1f;
        }
    }
}
