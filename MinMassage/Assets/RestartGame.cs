﻿using UnityEngine;
using System.Collections;

public class RestartGame : MonoBehaviour 
{
	void Update () 
    {
        if (Input.GetMouseButtonDown(0) || Input.GetButton("JumpOne") || Input.GetButton("JumpTwo"))
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }
	}
}
