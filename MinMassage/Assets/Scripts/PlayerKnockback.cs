﻿using UnityEngine;
using System.Collections;

public enum KnockbackDirections
{
    Left,
    Right
};

public class PlayerKnockback : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.transform.tag == "Player" && col.transform.GetComponent<PlayerController2>().Dashing)
        {
            SendMessage("Knockback", col.transform.rigidbody2D.velocity);
        }
    }
}
