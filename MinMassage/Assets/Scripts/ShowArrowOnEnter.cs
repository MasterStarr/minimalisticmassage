﻿using UnityEngine;
using System.Collections;

public class ShowArrowOnEnter : MonoBehaviour 
{
    public string PlayerSide = "One";

    public GameObject arrow1;
    public GameObject arrow2;
    public float enabledTime = 1f;

    void OnTriggerEnter2D(Collider2D col)
    {
        DoDrawArrow(col);
    }

    public void DoDrawArrow(Collider2D col)
    {
        if (col.tag == "Player")
        {
            DisableAllArrows.instance.DisableArrows();

            PlayerController2 player = col.GetComponent<PlayerController2>();

            if (player.Role == PlayerRoles.Attacker)
            {
                if (player.PlayerName != PlayerSide && arrow1 != null)
                {
                    StartCoroutine("DisableArrow", arrow1);
                }
                else if (player.PlayerName == PlayerSide && arrow2 != null)
                {
                    StartCoroutine("DisableArrow", arrow2);
                }
            }
        }
    }

    public void DoDrawArrowDeath(Collider2D col)
    {
        if (col.tag == "Player")
        {
            PlayerController2 player = col.GetComponent<PlayerController2>();

            if (player.Role == PlayerRoles.Attacker)
            {
                if (player.PlayerName == PlayerSide && arrow1 != null)
                {
                    StartCoroutine("DisableArrow", arrow1);
                }
                else if (player.PlayerName != PlayerSide && arrow2 != null)
                {
                    StartCoroutine("DisableArrow", arrow2);
                }
            }
        }
    }

    IEnumerator DisableArrow(GameObject arrow)
    {
        arrow.active = true;

        yield return new WaitForSeconds(enabledTime);

        arrow.active = false;
    }
}
