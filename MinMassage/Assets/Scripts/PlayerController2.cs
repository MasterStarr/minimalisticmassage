﻿using UnityEngine;
using System.Collections;

public enum PlayerRoles
{
    Attacker,
    Defender,
    Undecided
};

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController2 : MonoBehaviour
{
#region Variables
    [HeaderAttribute("Standard Variables")]
    public string
        PlayerName          = "one";

    public float 
        MaxSpeed            = 5f,
        Acceleration        = 5f,
        Deceleration        = 5f,
        AirDeceleration     = 5f,
        JumpHeight          = 5f,
        DashSpeed           = 5f,
        MinDashDelay        = 1.5f,
        StunTime            = 1.5f,
        DashTime            = 5f; 

    public bool
        HasControl      = true,
        Dashing         = false;

    public PlayerRoles Role = PlayerRoles.Undecided;

    [HeaderAttribute("Object References")]

    public Transform[] GroundCheckPos;
    public Transform[] LeftCheckPos;
    public Transform[] RightCheckPos;

    public Vector2 WallJumpForce = Vector2.one;

    public GameObject BloodPrefab;
    public GameObject BloodPrefabLeft;
    public GameObject BloodPrefabRight;
    public Transform BloodSpawnLocationGround;
    public Transform BloodSpawnLocationLeft;
    public Transform BloodSpawnLocationRight;

    public GameObject DashObject;
    public GameObject SpawnObject;
    public GameObject PlayerArt;

    private float
        CurSpeed            = 0f,
        LastJumpTime        = -100f,
        LastWallJumpTime    = -100f,
        LastDashTime        = -100f,
        MinJumpDelay        = 0.25f,
        MinWallJumpDelay    = .35f;

    private Vector2
        PlayerInput01           = Vector2.zero,
        PlayerInputSnap         = Vector2.zero,
        Movement                = Vector2.zero,
        movementDirection       = Vector2.zero,
        LastMovementDirection   = Vector2.zero,
        ExternalForces          = Vector2.zero;

    private bool
        Grounded    = false,
        CanWallJump = false,
        OnLeftWall  = false,
        CanJump     = false,
        CanDash     = false,
        OnRightWall = false;

    private Side ValidSide = Side.Any;
    private Side LastSide = Side.Any;

    private Vector2 MovementDirection
    {
        get { return movementDirection; }
        set
        {
            LastMovementDirection = movementDirection;
            movementDirection = value;
        }
    }

    #region events

    public delegate void PlayerDeath(PlayerController2 player);
    public static event PlayerDeath Death;

    public delegate void PlayerJump();
    public static event PlayerJump Jump;

    public delegate void PlayerDash();
    public static event PlayerDash Dash;

    #endregion

#endregion

    void Start()
    {
        Physics2D.raycastsHitTriggers = false;
    }

    bool CheckWallJump()
    {
        bool left =  LinecastArray(LeftCheckPos, "Ground");
        OnLeftWall = left;
        left = left && (LastSide == Side.Right || LastSide == Side.Any);

        bool right = LinecastArray(RightCheckPos, "Ground");
        OnRightWall = right;
        right = right && (LastSide == Side.Right || LastSide == Side.Any);

        if (Grounded)
        {
            ValidSide = Side.Any;
            return false;
        }

        if (left && right)
        {
            return true;
        }
        else if (left)
        {
            return true;
        }
        else if(right)
        {
            return true;
        }

        return false;
    }

    bool LinecastArray(Transform[] positions, string _tag)
    {
        foreach (Transform pos in positions)
        {
            RaycastHit2D hit = Physics2D.Linecast(transform.position, pos.position);

            if (hit.transform != null && hit.transform.tag == _tag)
            {
                return true;
            }
        }

        return false;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        //if(col.transform.tag == "Player" && col.transform.GetComponent<PlayerController2>().Dashing)
        //{
        //    Die();
        //}
    }

    void Update()
    {
        if (HasControl)
        {
            //  Set the movement based on the players input
            PlayerInput01 = new Vector2(
                Mathf.RoundToInt(Input.GetAxisRaw("Horizontal" + PlayerName)), 
                Mathf.RoundToInt(Input.GetAxisRaw("Vertical" + PlayerName)));
            PlayerInputSnap = new Vector2(
                RoundToNearest(Input.GetAxisRaw("Horizontal" + PlayerName), 1f),
                RoundToNearest(Input.GetAxisRaw("Vertical" + PlayerName), 1f, -1f, 0f));

            //  Check if the player is grounded
            Grounded = LinecastArray(GroundCheckPos, "Ground");
            //  Set which side can be used to wall jump
            CanWallJump = CheckWallJump();

            if (Grounded)
                CanDash = true;

            //  If the player is attempting to move, Increase the CurSpeed
            if (PlayerInput01.x == 1f)
            {
                rigidbody2D.AddForce(new Vector2(Acceleration, 0));
                MovementDirection = PlayerInput01;
            }
            else if (PlayerInput01.x == -1f)
            {
                rigidbody2D.AddForce(new Vector2(-Acceleration, 0));
                MovementDirection = PlayerInput01;
            }
            else
            {
                if (Grounded)
                {
                    Vector2 force = rigidbody2D.velocity * -Deceleration;
                    force.y = 0f;
                    rigidbody2D.AddForce(force);
                }
                else
                {
                    Vector2 force = rigidbody2D.velocity * -AirDeceleration;
                    force.y = 0f;
                    rigidbody2D.AddForce(force);
                }
            }

            //  Change movement direction
            if (LastMovementDirection.x == -MovementDirection.x && LastMovementDirection.x != 0f)
            {
                Vector2 force = -rigidbody2D.velocity / Time.fixedDeltaTime;
                force.y = 0f;
                rigidbody2D.AddForce(force);
            }

            if(DashDown() && CanDash && PlayerInputSnap != Vector2.zero && ValidDashInput() && Time.time - LastDashTime > MinDashDelay)
            {
                StartCoroutine("DoDash", DashTime);
                Dash();

                Vector2 scale = new Vector2(0.25f, 0.25f);
                if (PlayerInputSnap.x != 0)
                    scale.x = PlayerInputSnap.x / 4;
                if (PlayerInputSnap.y != 0)
                    scale.y = PlayerInputSnap.y / 4;

                DashObject.transform.localScale = scale;

                print(scale);

                DashObject.active = true;
            }

            //  Wall jump
            if (JumpDown() && CanWallJump && Time.time - LastWallJumpTime > MinWallJumpDelay && Time.time - LastJumpTime > MinJumpDelay)
            {
                LastWallJumpTime = Time.time;
                CanJump = false;

                if ((ValidSide == Side.Any || ValidSide == Side.Left) && OnLeftWall)
                {
                    MovementDirection = new Vector2(0, 0);
                    rigidbody2D.AddForce(WallJumpForce, ForceMode2D.Impulse);
                    ValidSide = Side.Right;
                    Jump();
                }

                else if ((ValidSide == Side.Any || ValidSide == Side.Right) && OnRightWall)
                {
                    MovementDirection = new Vector2(0, 0);
                    rigidbody2D.AddForce(new Vector2(-WallJumpForce.x, WallJumpForce.y), ForceMode2D.Impulse);
                    ValidSide = Side.Left;
                    Jump();
                }

                CurSpeed = MaxSpeed;
            }

            //  Regular jump
            if (JumpDown() && (Grounded) && Time.time - LastJumpTime > MinJumpDelay)
            {
                rigidbody2D.AddForce(new Vector2(0f, JumpHeight), ForceMode2D.Impulse);
                LastJumpTime = Time.time;
                CanJump = false;
                Jump();
            }

            if (rigidbody2D.velocity.x > MaxSpeed)
            {
                Vector2 vel = rigidbody2D.velocity;
                vel.x = MaxSpeed;
                rigidbody2D.velocity = vel;
            }
            else if (rigidbody2D.velocity.x < -MaxSpeed)
            {
                Vector2 vel = rigidbody2D.velocity;
                vel.x = -MaxSpeed;
                rigidbody2D.velocity = vel;
            }
        }
        else if(Dashing)
        {
            rigidbody2D.velocity = (PlayerInputSnap.normalized) * DashSpeed;
        }

        CheckWallJump();
        Grounded = LinecastArray(GroundCheckPos, "Ground");

        if (!HasControl)
        {
            if (OnRightWall)
            {
                Instantiate(BloodPrefabRight, BloodSpawnLocationRight.position, BloodSpawnLocationRight.rotation);
                Die();
            }
            else if(OnLeftWall)
            {
                Instantiate(BloodPrefabLeft, BloodSpawnLocationLeft.position, BloodSpawnLocationLeft.rotation);
                Die();
            }
            else if(Grounded && (PlayerInputSnap.y != 0))
            {
                Instantiate(BloodPrefab, BloodSpawnLocationGround.position, BloodSpawnLocationGround.rotation);
                Die();
            }
        }

        if (PlayerInputSnap.x < 0)
            PlayerArt.transform.localScale = new Vector2(.15f, .15f);
        if (PlayerInputSnap.x > 0)
            PlayerArt.transform.localScale = new Vector2(-.15f, .15f);
    }

    float RoundToNearest(float init, float multiple)
    {
        return Mathf.Ceil(init / multiple) * multiple;
    }

    float RoundToNearest(float init, float multiple, float min, float max)
    {
        float ret = Mathf.Ceil(init / multiple) * multiple;
        ret = Mathf.Clamp(ret, min, max);

        return ret;
    }

    bool ValidDashInput()
    {
        return (Grounded && PlayerInputSnap.y == 0) || !Grounded;
    }

    bool DashDown()
    {
        if (Input.GetButtonDown("Dash" + PlayerName))
            return true;

        return false;
    }

    bool JumpDown()
    {
        if (Input.GetButton("Jump" + PlayerName))
            return true;

        return false;
    }

    public void Die()
    {
        NullMovement();
        HasControl = true;

        if (Death != null)
            Death(this);
    }

    public void NullMovement()
    {
        rigidbody2D.velocity = Vector2.zero;
    }

    public void Knockback(Vector2 _vel)
    {
        StartCoroutine(DoKnockback(_vel));
    }

    IEnumerator DoKnockback(Vector2 velocity)
    {
        rigidbody2D.velocity = velocity;
        HasControl = false;

        yield return new WaitForSeconds(StunTime);

        HasControl = true;
    }

    IEnumerator DoDash(float time)
    {
        LastDashTime = Time.time;
        Dashing = true;
        HasControl = false;
        rigidbody2D.gravityScale = 0f;
        rigidbody2D.velocity = Vector2.zero;
        print(PlayerInputSnap);

        yield return new WaitForSeconds(time);

        rigidbody2D.gravityScale = 1f;
        HasControl = true;
        Dashing = false;
        rigidbody2D.velocity = PlayerInputSnap * MaxSpeed;
    }

    IEnumerator ResetExternalForces(float time)
    {
        yield return new WaitForSeconds(time);

        ExternalForces = Vector3.zero;
    }
}
