﻿using UnityEngine;
using System.Collections;

public class TempBlocks : MonoBehaviour 
{
    public PlayerController2 Player;

	void Update () 
    {
        if(Player.Role == PlayerRoles.Undecided)
        {
            transform.GetChild(0).active = true;
        }
        else
        {
            transform.GetChild(0).active = false;
        }
	}
}