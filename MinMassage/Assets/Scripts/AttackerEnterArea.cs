﻿using UnityEngine;
using System.Collections;

public class AttackerEnterArea : MonoBehaviour 
{
    public static float MinDelay = 1f;
    public static float LastEntrance = -100f;

    public static GameObject CurrentArea;
    public GameObject DefaultArea;

    public static bool init = false;

    void Start()
    {
        if(DefaultArea != null)
            CurrentArea = DefaultArea;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            if (!init)
            {
                CurrentArea = gameObject;
                print(CurrentArea);
                init = true;
            }

            if (Time.time - LastEntrance > MinDelay && col.GetComponent<PlayerController2>().Role == PlayerRoles.Attacker)
            {
                CurrentArea = gameObject;
                print(CurrentArea);
                //respawn defender Spawn
            }
        }
    }
}