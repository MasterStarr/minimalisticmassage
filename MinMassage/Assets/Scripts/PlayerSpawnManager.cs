﻿using UnityEngine;
using System.Collections;


public class PlayerSpawnManager : MonoBehaviour 
{
    public PlayerController2 Player1;
    public PlayerController2 Player2;
    public AudioSource SpawnSound;

	// Use this for initialization
	void Start () 
    {
        PlayerController2.Death += PlayerController2_Death;
	}

    void OnDestroy()
    {
        PlayerController2.Death -= PlayerController2_Death;
        print("endo");
    }

    void PlayerController2_Death(PlayerController2 player)
    {
        player.transform.position = AttackerEnterArea.CurrentArea.transform.GetChild(0).position;
        SpawnSound.Play();
        player.SpawnObject.active = true;
    }
}
