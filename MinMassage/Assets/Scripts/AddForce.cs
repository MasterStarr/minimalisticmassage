﻿using UnityEngine;
using System.Collections;

public class AddForce : MonoBehaviour 
{
    void ApplyForce(Vector2 force)
    {
        rigidbody2D.AddForce(force);
    }
}
