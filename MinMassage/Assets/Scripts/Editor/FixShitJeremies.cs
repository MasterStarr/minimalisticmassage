﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class FixJeremiesShit : ScriptableWizard
{
    public bool copyValues = true;
    public GameObject[] NewType;
    public GameObject[] OldObjects;

    [MenuItem("Custom/Fix Jeremies Shit")]


    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("Fix Jeremies Shit", typeof(FixJeremiesShit), "Replace");
    }

    void OnWizardCreate()
    {
        //Transform[] Replaces;
        //Replaces = Replace.GetComponentsInChildren<Transform>();

        foreach (GameObject go in OldObjects)
        {
            GameObject newObject;
            newObject = (GameObject)PrefabUtility.InstantiatePrefab(NewType[Random.Range(0, NewType.Length)]); 
            newObject.transform.position = go.transform.position;
            newObject.transform.rotation = go.transform.rotation;
            newObject.transform.parent = go.transform.parent;

            DestroyImmediate(go);
        }
    }
}
