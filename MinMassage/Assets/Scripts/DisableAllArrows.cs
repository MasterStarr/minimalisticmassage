﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisableAllArrows : MonoBehaviour 
{
    public List<GameObject> Arrows = new List<GameObject>();
    public static DisableAllArrows instance;

    void Start()
    {
        instance = this;
    }

    public void DisableArrows()
    {
        foreach(GameObject arrow in Arrows)
        {
            arrow.active = false;
        }
    }
}
