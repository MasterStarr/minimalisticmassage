﻿using UnityEngine;
using System.Collections;

public class PlayerRoleManager : MonoBehaviour
{
    public PlayerController2 Player1;
    public PlayerController2 Player2;

    void Start()
    {
        PlayerController2.Death += PlayerController2_Death;
    }

    void OnDestroy()
    {
        PlayerController2.Death -= PlayerController2_Death;
    }

    void PlayerController2_Death(PlayerController2 player)
    {
        StartCoroutine("SwapRoles", player);
    }

    IEnumerator SwapRoles(PlayerController2 player)
    {
        yield return new WaitForSeconds(0.1f);

        if (player == Player1)
        {
            if (player.Role == PlayerRoles.Undecided)
            {
                Player1.Role = PlayerRoles.Defender;
                Player2.Role = PlayerRoles.Attacker;
            }
            else if (player.Role == PlayerRoles.Attacker)
            {
                Player1.Role = PlayerRoles.Defender;
                Player2.Role = PlayerRoles.Attacker;
            }
        }
        else if (player == Player2)
        {
            if (player.Role == PlayerRoles.Undecided)
            {
                Player2.Role = PlayerRoles.Defender;
                Player1.Role = PlayerRoles.Attacker;
            }
            else if (player.Role == PlayerRoles.Attacker)
            {
                Player2.Role = PlayerRoles.Defender;
                Player1.Role = PlayerRoles.Attacker;
            }
        }
    }
}
