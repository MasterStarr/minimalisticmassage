﻿using UnityEngine;
using System.Collections;

public class KillOnCollision : MonoBehaviour 
{
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.transform.tag == "Player")
        {
            col.transform.GetComponent<PlayerController2>().Die();
        }
    }
}
