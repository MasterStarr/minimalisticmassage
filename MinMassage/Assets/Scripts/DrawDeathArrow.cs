﻿using UnityEngine;
using System.Collections;

public class DrawDeathArrow : MonoBehaviour 
{
	void Start () 
    {
        PlayerController2.Death += PlayerController2_Death;
	}

    void OnDestroy()
    {
        PlayerController2.Death -= PlayerController2_Death;
    }

    void PlayerController2_Death(PlayerController2 player)
    {
        print("one");
        DisableAllArrows.instance.DisableArrows();

        if (player.Role == PlayerRoles.Attacker)
        {
            print("dos");
            if(AttackerEnterArea.CurrentArea.GetComponent<ShowArrowOnEnter>() != null)
            {
                print("tres");
                AttackerEnterArea.CurrentArea.GetComponent<ShowArrowOnEnter>().DoDrawArrowDeath(player.collider2D);
            }
        }
    }
}
