﻿using UnityEngine;
using System.Collections;

public enum Side
{
    Left,
    Right,
    Any
}

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
#region Variables

    public Transform[] GroundCheckPos;
    public Transform[] LeftCheckPos;
    public Transform[] RightCheckPos;

    public Vector2 WallJumpForce = Vector2.one;

    public float 
        Speed               = 5f,
        MaxSpeed            = 5f,
        Acceleration        = 5f,
        Deceleration        = 5f,
        AirDeceleration     = 5f,
        JumpHeight          = 5f,
        WallJumpHeight      = 5f,
        DashSpeed           = 5f,
        DashTime            = 5f,
        WallForceFalloff    = 6f;

    private float
        CurSpeed            = 0f,
        LastJumpTime        = -100f,
        LastWallJumpTime    = -100f,
        MinJumpDelay        = 0.25f,
        MinWallJumpDelay    = .35f;

    private Vector2
        PlayerInput             = Vector2.zero,
        Movement                = Vector2.zero,
        movementDirection       = Vector2.zero,
        LastMovementDirection   = Vector2.zero,
        ExternalForces          = Vector2.zero;

    private bool
        Grounded    = false,
        CanWallJump = false,
        OnLeftWall  = false,
        CanJump     = false,
        OnRightWall = false;

    private Side ValidSide = Side.Any;
    private Side LastSide = Side.Any;

    private Vector2 MovementDirection
    {
        get { return movementDirection; }
        set
        {
            LastMovementDirection = movementDirection;
            movementDirection = value;
        }
    }

#endregion

    void Start()
    {

    }

    bool CheckWallJump()
    {
        bool left =  LinecastArray(LeftCheckPos, "Ground");
        OnLeftWall = left;
        left = left && (LastSide == Side.Right || LastSide == Side.Any);

        bool right = LinecastArray(RightCheckPos, "Ground");
        OnRightWall = right;
        right = right && (LastSide == Side.Right || LastSide == Side.Any);

        if (Grounded)
        {
            ValidSide = Side.Any;
            return false;
        }

        if (left && right)
        {
            return true;
        }
        else if (left)
        {
            return true;
        }
        else if(right)
        {
            return true;
        }

        return false;
    }

    bool LinecastArray(Transform[] positions, string _tag)
    {
        foreach (Transform pos in positions)
        {
            RaycastHit2D hit = Physics2D.Linecast(transform.position, pos.position);

            if (hit.transform != null && hit.transform.tag == _tag)
            {
                return true;
            }
        }

        return false;
    }

    void Update()
    {
        //  Set the movement based on the players input
        PlayerInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        //  Check if the player is grounded
        Grounded = LinecastArray(GroundCheckPos, "Ground");
        //  Set which side can be used to wall jump
        CanWallJump = CheckWallJump();

        if (Grounded)
            CanJump = true;

        //  If the player is attempting to move, Increase the CurSpeed
        if (PlayerInput.x == 1f)
        {
            CurSpeed = Mathf.Clamp(CurSpeed + Acceleration * Time.deltaTime, 0, MaxSpeed);
            MovementDirection = PlayerInput;
        }
        else if (PlayerInput.x == -1f)
        {
            CurSpeed = Mathf.Clamp(CurSpeed + Acceleration * Time.deltaTime, 0, MaxSpeed);
            MovementDirection = PlayerInput;
        }
        else
        {
            if (Grounded)
                CurSpeed = Mathf.Clamp(CurSpeed - Deceleration * Time.deltaTime, 0, MaxSpeed);
            else
                CurSpeed = Mathf.Clamp(CurSpeed - AirDeceleration * Time.deltaTime, 0, MaxSpeed);
        }

        //  Change movement direction
        if (LastMovementDirection.x == -MovementDirection.x)
        {
            //CurSpeed = 0f;
        }

        //  Wall jump
        if (PlayerInput.y == 1f && CanWallJump && Time.time - LastWallJumpTime > MinWallJumpDelay && Time.time - LastJumpTime > MinJumpDelay)
        {
            LastWallJumpTime = Time.time;
            CanJump = false;

            if ((ValidSide == Side.Any || ValidSide == Side.Left) && OnLeftWall)
            {
                MovementDirection = new Vector2(1f, 0f);
                rigidbody2D.AddForce(WallJumpForce, ForceMode2D.Impulse);
                ValidSide = Side.Right;
            }

            else if ((ValidSide == Side.Any || ValidSide == Side.Right) && OnRightWall)
            {
                MovementDirection = new Vector2(-1f, 0f);
                rigidbody2D.AddForce(WallJumpForce, ForceMode2D.Impulse);
                ValidSide = Side.Left;
            }

            CurSpeed = MaxSpeed;
        }

        //  Regular jump
        if(PlayerInput.y == 1f && (Grounded || CanJump) && Time.time - LastJumpTime > MinJumpDelay)
        {
            rigidbody2D.AddForce(new Vector2(0f, JumpHeight), ForceMode2D.Impulse);
            LastJumpTime = Time.time;
            CanJump = false;
        }

        //  Set movement based on player input and external sources
        Movement.x = ((MovementDirection) * CurSpeed).x + ExternalForces.x;
        Movement.y = (rigidbody2D.velocity).y + ExternalForces.y;

        rigidbody2D.velocity = Movement;
    }

    IEnumerator ResetExternalForces(float time)
    {
        yield return new WaitForSeconds(time);

        ExternalForces = Vector3.zero;
    }
}
