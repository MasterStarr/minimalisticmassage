﻿using UnityEngine;
using System.Collections;

public class PlayerStomp : MonoBehaviour 
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" && !(col.GetComponent<PlayerController2>().Dashing || transform.root.GetComponent<PlayerController2>().Dashing))
        {
            print("death to " + transform.root.name);
            SendMessageUpwards("Die");
        }
    }
}
