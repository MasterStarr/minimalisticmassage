﻿using UnityEngine;
using System.Collections;

public class AnimationEvents : MonoBehaviour 
{
    void Kill()
    {
        GameObject.Destroy(transform.root.gameObject);
    } 

    void Pause()
    {
        GetComponent<Animator>().enabled = false;
    }

    void Disable()
    {
        gameObject.active = false;
    }
}
 