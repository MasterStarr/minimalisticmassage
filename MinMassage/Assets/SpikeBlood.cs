﻿using UnityEngine;
using System.Collections;

public class SpikeBlood : MonoBehaviour 
{
    public GameObject SpikeBloodPrefab;

    public void OnCollisionEnter2D(Collision2D col)
    {
        if(col.transform.tag == "Player")
        {
            GameObject go = (GameObject)Instantiate(SpikeBloodPrefab, transform.position, transform.rotation);
            Vector3 scale = go.transform.localScale;
            scale.x *= transform.root.localScale.x;
            scale.y *= transform.root.localScale.y;
            go.transform.localScale = scale;
        }
    }
}
