﻿using UnityEngine;
using System.Collections;

public class ManageSounds : MonoBehaviour 
{
    public AudioSource JumpSound;
    public AudioSource DeathSound;
    public AudioSource DashSound;
    public AudioSource WinSound;    

    void Start()
    {
        PlayerController2.Dash += PlayerController2_Dash;
        PlayerController2.Death += PlayerController2_Death;
        PlayerController2.Jump += PlayerController2_Jump;
        WinGame.Win += WinGame_Win;

        print("starto");
    }

    void OnDestroy()
    {
        PlayerController2.Dash -= PlayerController2_Dash;
        PlayerController2.Death -= PlayerController2_Death;
        PlayerController2.Jump -= PlayerController2_Jump;
        WinGame.Win -= WinGame_Win;
    }

    void WinGame_Win()
    {
        WinSound.Play();
    }

    void PlayerController2_Jump()
    {
        JumpSound.Play();
    }

    void PlayerController2_Death(PlayerController2 player)
    {
        DeathSound.Play();
    }

    void PlayerController2_Dash()
    {
        DashSound.Play();
    }
}
