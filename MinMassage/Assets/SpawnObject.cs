﻿using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour 
{
    public GameObject go;
    public string Side;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player" && col.GetComponent<PlayerController2>().PlayerName == Side && col.GetComponent<PlayerController2>().Role == PlayerRoles.Attacker)
            Instantiate(go);
    }
}
